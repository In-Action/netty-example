package net.j4love.netty.inaction.c8;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.oio.OioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.oio.OioDatagramChannel;
import org.junit.Test;

import java.net.InetSocketAddress;

/**
 * @author he peng
 * @create 2018/6/14 11:19
 * @see
 */
public class DatagramChannelTest {

    @Test
    public void datagramChannelTest0() {
        OioEventLoopGroup group = new OioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.group(group)
                    .channel(OioDatagramChannel.class)
                    .handler(new SimpleChannelInboundHandler<DatagramPacket>() {
                        @Override
                        protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {

                        }
                    });
            ChannelFuture future = bootstrap.bind(new InetSocketAddress("127.0.0.1", 6667));
            future.addListener((ChannelFutureListener) channelFuture -> {
                if (channelFuture.isSuccess()) {
                    System.out.println("Channel bound");
                } else {
                    System.err.println("Channel Bind attempt failed");
                    channelFuture.cause().printStackTrace();
                }
            });
        } finally {
            try {
                group.shutdownGracefully().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
