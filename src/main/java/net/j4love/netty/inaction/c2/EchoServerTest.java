package net.j4love.netty.inaction.c2;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.junit.Test;

import java.net.InetSocketAddress;

/**
 * @author he peng
 * @create 2018/6/12 11:45
 * @see
 */
public class EchoServerTest {

    static final InetSocketAddress SERVER_ADDRESS = new InetSocketAddress("127.0.0.1" , 9999);

    @Test
    public void openEchoServerTest() throws Exception {

        final EchoServerHandler echoServerHandler = new EchoServerHandler();
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup , workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .localAddress(SERVER_ADDRESS)
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                // 当有一个新的连接建立时 , 将 EchoServerHandler 实例添加到该 Channel 的 ChannelPipeline 上
                                System.out.println("[" + Thread.currentThread().getName() + "] Thread , " +
                                        "New Connected Channel -> " + ch.remoteAddress());
                                ch.pipeline().addLast("EchoServerHandler" , echoServerHandler);
                            }
                        });
            ChannelFuture future = serverBootstrap.bind().sync();
            System.out.println("********************* [" + Thread.currentThread().getName() + "] Thread , " +
                    "echo server start in " + SERVER_ADDRESS.getPort() + " port *********************");
            future.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully().sync();
            workerGroup.shutdownGracefully().sync();
        }
    }

    @Test
    public void openEchoClientTest() throws Exception {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .remoteAddress(SERVER_ADDRESS)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new EchoClientHandler());
                        }
                    });

            // sync() 会阻塞当前线程直到操作完成
            ChannelFuture future = bootstrap.connect().sync();
            future.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }

}
