

##################################### Netty IN Action 第 3 章 Netty的组件和设计 阅读笔记 #####################################

Channel —— Socket
EventLoop —— 控制流、多线程处理、并发
ChannelFuture —— 异步通知

Channel 接口
    基本的 I/O 操作（bind(), connect() , read() , write() ）依赖于底层网络传输所提供的原语。在基于 Java 的网络编程中，其基本
的构造是class Socket 。netty 提供的 Channel 接口 大大降低了直接使用 Socket 类的复杂性。


EventLoop 接口
    EventLoop 定义了 netty 的核心抽象，用于处理连接的生命周期中所发生的事件。
    一个 EventLoopGroup 包含一个或者多个 EventLoop ;
    一个 EventLoop 在它的生命周期内只和一个 Thread 绑定 ;
    所有的 EventLoop 处理的 I/O 事件都将在它专有的 Thread 上被处理 ;
    一个 Channel 在它的生命周期内只注册于一个 EventLoop 上 ；
    一个 EventLoop 可能被分配给一个或多个 Channel 。

    注意在这种设计中，一个给定 Channel 的 I/O 操作都是由一个 Thread 去执行 ， 所以不存在并发安全问题，消除了对同步的需要。


ChannelFuture 接口

